The 'Signup Confirm From Email' module signs user up for a node from a secure
link. The link is sent to user's e-mail address. The module was made as a result
of a client requirement for an anonymous visitors-only site. To prevent spamming
of the 'signup_log' table, e-mail confirmation is required.

Installation:
1. Place the whole directory in sites/all/modules/
2. Navigate to 'Administer', 'Site building', 'Modules' and enable 'Signup
Confirm From Email'.

This module requires 'Signup' module (http://drupal.org/project/signup).
